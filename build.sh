#!/usr/bin/env bash

# Install dependencies.
go get github.com/gin-gonic/gin
go get github.com/gin-contrib/cors

# Build app
go build -o bin/application portal/application.go
