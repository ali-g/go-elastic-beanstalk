package main

// required packages
import (
    "fmt"

    "github.com/gin-gonic/gin"
    "github.com/gin-contrib/cors"
)


func serveApplication() {
    corsConfig := cors.Config {
        AllowOrigins: []string{"http://localhost:3000"},
        AllowMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
        AllowHeaders: []string{"Origin", "Content-Type", "Authorization"},
        AllowCredentials: true,
    }

    router := gin.Default()
    router.Use(cors.New(corsConfig))
    router.StaticFile("/", "./public/index.html")
    router.Run(":5000")
    fmt.Println("Server running on port 5000")
}

func main() {
    // start the server
    serveApplication()

}
